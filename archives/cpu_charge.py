import paramiko


def get_remote_cpu_usage(host, port, username):
    # Initialize SSH client
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(host, port, username)

    command = '''top -bn1 | grep "Cpu(s)" | awk -F'id,' '{ split($1, vs, ","); v=vs[length(vs)]; sub("%", "", v); printf "%0.1f", 100 - v }' '''
    stdin, stdout, stderr = client.exec_command(command)
    output = stdout.read().decode().strip()
       
    client.close()

    return output

# Example usage:
host_oates = "oates.ess.uci.edu"
host_hobbs = "hobbs.ess.uci.edu"
host_bakutis = "bakutis.ess.uci.edu"
port = 22  # Default SSH port
username = "jeremie"


cpu = get_remote_cpu_usage(host_oates, port, username)

if cpu is not None:
    print(f"CPU Usage: {cpu}%")
else:
    print("Could not retrieve CPU usage.")
