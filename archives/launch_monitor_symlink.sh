#!/bin/bash

# Check if a directory is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

# Ensure the provided path is a directory
if [ ! -d "$1" ]; then
    echo "Error: $1 is not a directory."
    exit 1
fi

# Initialize counters
symlink_count=0
real_file_count=0
total_count=0

# Loop through all files and directories under the specified directory
while IFS= read -r -d '' item; do
    # Check if it's a symlink
    if [ -L "$item" ]; then
        ((symlink_count++))
        echo "$item"
    # Check if it's a regular file
    elif [ -f "$item" ]; then
        ((real_file_count++))
    fi
    ((total_count++)
done < <(find "$1" -print0)

# Print the counts
echo "Total symlinks found: $symlink_count"
echo "Total real files found: $real_file_count"
echo "Total files:$total_count" 

exit 0
