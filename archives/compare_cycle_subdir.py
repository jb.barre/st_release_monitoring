import os

def get_immediate_subdirectories(path):
    """Return a list of immediate subdirectories for the given path."""
    return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]

def compare_directories(dir1, dir2):
    """Compare the immediate subdirectories of two directories."""
    subdirs1 = set(get_immediate_subdirectories(dir1))
    subdirs2 = set(get_immediate_subdirectories(dir2))
    
    if subdirs1 == subdirs2:
        return True, None
    else:
        missing_in_dir1 = subdirs2 - subdirs1
        missing_in_dir2 = subdirs1 - subdirs2
        return False, (missing_in_dir1, missing_in_dir2)

# Directories to compare
DIR1 = "/u/hobbs-r1/eric/SENTINEL1_greenland/2023/12d/Track170"
DIR2 = "/u/hobbs-r1/eric/SENTINEL1_greenland/2023_rescue/12d/Track170"

same, diff = compare_directories(DIR1, DIR2)

if same:
    print("The directories have the same subdirectories.")
else:
    print("The directories have different subdirectories:")
    if diff[0]:
        print(f"Missing in {DIR1}: {', '.join(diff[0])}")
    if diff[1]:
        print(f"Missing in {DIR2}: {', '.join(diff[1])}")
