#!/bin/bash

# Check if a directory is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

#Ensure the provided path is a directory
if [ ! -d "$1" ]; then
    echo "Error: $1 is not a directory."
    exit 1
fi
# Initialize counters
symlink_count=0
real_directory_count=0

# Loop through all directories under the specified directory
while IFS= read -r -d '' item; do
    # Check if it's a symlink
    if [ -L "$item" ]; then
        ((symlink_count++))
        echo "$item is a symlink."
    # Check if it's a real directory (and not a symlink)
    elif [ -d "$item" ]; then
        ((real_directory_count++))
        echo "$item is a real directory."
    fi
done < <(find "$1" -type d -print0)  # Note the -type d, which limits the results to directories

# Print the counts
echo "Total symlinks found: $symlink_count"
echo "Total real directories found: $real_directory_count"
