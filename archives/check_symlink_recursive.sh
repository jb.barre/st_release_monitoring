#!/bin/bash

# Check if a directory argument is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <parent_directory>"
    exit 1
fi

parent_dir="$1"

# Check if the directory exists
if [ ! -d "$parent_dir" ]; then
    echo "$parent_dir is not a directory!"
    exit 2
fi

# Use find to list all items and check if they are symlinked directories
find "$parent_dir" -type l | while read -r potential_symlink; do
    if [ -d "$(readlink -f "$potential_symlink")" ]; then
        echo "$potential_symlink is a symbolic link that points to a directory: $(readlink "$potential_symlink")."
    fi
done

exit 0

