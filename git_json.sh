# activate ssh key jbb
eval $(ssh-agent -s)
ssh-add ~/.ssh/id_rsa_jbb

# navigate to the directory containing the repository
cd ~/st_release_monitoring/s1_dashboard/docs/
git add s1_antarctica_2023_12d_dates.json s1_greenland_2023_12d_dates.json
git add s1_antarctica_2023_12d_check.json s1_greenland_2023_12d_check.json
# Commit the changes
git commit -m "upudate json files"

# Push the changes to the remote repository
git push origin master
