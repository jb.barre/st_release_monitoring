#!/bin/bash

cd /u/oates-r0/eric/SENTINEL1
echo ""
echo "====  SLC | AMPCOR  ======================================"
#loop through the numbers 1 to 5
for i in {1..5}; do

  # Define the file name based on the current iteration
  file_name="output_ant${i}_2022"
 
  # Assign server name based on the value of i
  case $i in
    1) server_name="mawson " ;;
    2) server_name="gp     " ;;
    3) server_name="bakutis" ;;
    4) server_name="pennell" ;;
    5) server_name="local  " ;;
  esac

  # Check if the file exists
  if [ -f "$file_name" ]; then
    # Capture the output of the command
    output=$(cat "$file_name" | grep -w '7267' | tail -n 1)
    # Prepend the captured output with "proc_antN : " and echo it
    echo "proc_ant${i} - $server_name : $output"
    else
    echo "File $file_name not found"
fi  
done

ssh -q jeremie@oates.ess.uci.edu ps -ax | grep s1_proc2 | grep -v grep | awk -F'destination=' '{print $2}' | awk '{print "proc2 running on", $1}'

echo ""
echo "==== offset map | interferogram | calibration ============"

# Loop through the numbers 1 to 5
for i in {1..6}; do
  # Define the file name based on the current iteration
  file_name="outputjerant${i}_2022"

  # Assign server name based on the value of i
  case $i in
    1) server_name="mawson " ;;
    2) server_name="gp     " ;;
    3) server_name="bakutis" ;;
    4) server_name="pennell" ;;
    5) server_name="local  " ;;
  esac

  # Check if the file exists
  if [ -f "$file_name" ]; then
    # Capture the output of the command
    output=$(cat "$file_name" | grep -w '7267' | tail -n 1)
    # Prepend the captured output with "proc_antN : " and echo it
    echo "calib_ant${i} - $server_name: $output"
    else
    echo "File $file_name not found"
fi
done

ssh -q jeremie@oates.ess.uci.edu  ps -ax | grep jer_ant | grep -v grep | awk -F'destination=' '{print $2}' | awk '{print "s1_jer_ant running on", $1}'
echo ""
echo "====  Disk usage ========================================="
disk_space=$(df -h | grep oates-r0)
echo "Disk space on $disk_space"
cd /u/mawson-r2
disk_space=$(df -h | grep mawson-r2)
echo "Disk space on $disk_space"
cd /u/hobbs-r1
disk_space=$(df -h | grep hobbs-r1)
echo "Disk space on $disk_space"
cd /u/baku-z1
disk_space=$(df -h | grep baku-z1)
echo "Disk space on $disk_space"
cd /u/hobbs-r1
disk_space=$(df -h | grep hobbs-r1)
echo "Disk space on $disk_space" 
