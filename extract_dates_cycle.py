import os
import re
import json
import argparse

def extract_info_from_file(file_path,vel):
    date_pattern = re.compile(r"_([0-9]{8})T")
    dates = []

    with open(file_path, "r") as f:
        for line in f:
            match = date_pattern.search(line)
            if match:
                date_str = match.group(1)
                formatted_date = f"{date_str[2:4]}-{date_str[4:6]}-{date_str[6:8]}"
                dates.append(formatted_date)

    if len(dates) < 2:
        return None

    pair_name = os.path.basename(os.path.dirname(file_path))
    return {"pairs": pair_name, "date1": dates[0], "date2": dates[1],"velocities":vel}


if __name__ == "__main__":
    # Define parser and arguments
    parser = argparse.ArgumentParser(
        description="Run status check based on files with given year,cycle and Ice Sheet."
    )
    parser.add_argument("year", type=str, help="Path to the list")
    parser.add_argument("cycle", type=str, help="cycle to monitor")
    parser.add_argument(
        "icesheet",
        choices=["gre", "greenland", "ant", "antarctica"],
        help="Ice sheet to monitor: gre or ant",
    )
    args = parser.parse_args()

    icesheet_dirs = {
        "gre":"/u/hobbs-r1/eric/SENTINEL1_greenland",
        "greenland":"/u/hobbs-r1/eric/SENTINEL1_greenland",
        "ant":"/u/oates-r0/eric/SENTINEL1/",
        "antarctica":"/u/oates-r0/eric/SENTINEL1/",
    }

    icesheet_file = {
        "gre":"s1_greenland_{year}_12d_dates.json".format(year=args.year),
        "greenland":"s1_greenland_{year}_12d_dates.json".format(year=args.year),
        "ant":"s1_antarctica_{year}_12d_dates.json".format(year=args.year),
        "antarctica":"s1_antarctica_{year}_12d_dates.json".format(year= args.year)   
    }

    results = []

    dir_path = os.path.join(icesheet_dirs[args.icesheet], args.year, args.cycle)
    print(dir_path)
    # Traverse directories and subdirectories to find all default_param.par files
    for root, dirs, files in os.walk(dir_path):
        #vel: check if velocities processed
        vel=0
                # Check if the directory also contains a file named "default_param.par"
        if "default_param.par" in files:
            if any(re.search(r"Fig1_[0-9]+\.jpg", file) for file in files):
                vel = 1
            full_path = os.path.join(root, "default_param.par")
            info = extract_info_from_file(full_path,vel)
            if info:
                results.append(info)

    # Store results in a JSON file
    with open(
        os.path.join("/home","jeremie","st_release_monitoring","s1_dashboard","docs", icesheet_file[args.icesheet]), "w"
    ) as outfile:
        json.dump(results, outfile, indent=4)

    print(f"Extracted information saved in {os.path.join('s1_dashboard','docs', icesheet_file[args.icesheet])}")
