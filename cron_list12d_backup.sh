#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/jeremie/st_release_monitoring


# Generate a timestamp in the format YYYY-MM-DD_HH-MM-SS
TIMESTAMP=$(date +"%Y-%m-%d_%H-%M-%S")

cd /u/hobbs-r1/eric/SENTINEL1_greenland/2023/12d
cp list12d.sav list_backup/list12d_backup_$TIMESTAMP.sav

cd /u/oates-r0/eric/SENTINEL1/2023/12d/
cp list12d.sav list_backup/list12d_backup_$TIMESTAMP.sav

cd /u/hobbs-r1/eric/SENTINEL1_greenland/2022/12d
cp list12d.sav list_backup/list12d_backup_$TIMESTAMP.sav

cd /u/oates-r0/eric/SENTINEL1/2022/12d/
cp list12d.sav list_backup/list12d_backup_$TIMESTAMP.sav

