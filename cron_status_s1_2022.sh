#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/home/jeremie/st_release_monitoring



source $HOME/miniconda3/etc/profile.d/conda.sh
conda activate s1_chain
env > $HOME/st_release_monitoring/cron_env.txt
# run the script to fectch dates of processed images
python $HOME/st_release_monitoring/extract_dates_cycle.py 2022 12d greenland >> $HOME/st_release_monitoring/cron_outputs/greenland_2022_12d_dates_cron.txt 2>&1
python $HOME/st_release_monitoring/extract_dates_cycle.py 2022 12d antarctica >> $HOME/st_release_monitoring/cron_outputs/antarctica_2022_12d_dates_cron.txt 2>&1
# Run the python script and redirect output to the specified text file
python $HOME/ST_RELEASE/COMMON/PYTHON/launch_status_find_s1.py 2022 12d greenland  hobbs >> $HOME/st_release_monitoring/cron_outputs/greenland_2022_12d_check_cron.txt
python $HOME/ST_RELEASE/COMMON/PYTHON/launch_status_find_s1.py 2022 12d antarctica oates >> $HOME/st_release_monitoring/cron_outputs/antarctica_2022_12d_check_cron.txt

# Deactivate the conda environment (optional, but good practice)

conda deactivate
conda activate base

